﻿using System.Collections;
using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Base Projectile data and behaviour. Provides per projectile type data to weapon
    /// and manages the lifetime, physics interactions of the projectile itself.
    ///
    /// Note, you may wish to separate the data from the behaviour
    /// </summary>
    public class Projectile : MonoBehaviour
    {
        [Tooltip("Desired max instantions of this projectile by the weapon per second.")]
        [SerializeField] protected float fireRate;

        [Tooltip("Desired starting speed of this projectile, units per second")]
        [SerializeField] protected float speed;

        [Tooltip("Uses RB for movement, if not is supplied here, GetComponent done in Start.")]
        [SerializeField] protected Rigidbody2D rb;

        [Tooltip("Lifetime of the projectile.")]
        [SerializeField] protected float TTL = 1;

        [Tooltip("Effects to be instantiated, as indicated by name.")]
        [SerializeField] protected GameObject spawnEffect, despawnEffect, impactEffect;

        [Tooltip("Tags we attempt to deal damage to.")]
        [SerializeField] protected TagCollection dealDamageTags;

        [Tooltip("Tags we impact but do not attempt to deal damage to.")]
        [SerializeField] protected TagCollection impactTags;

        [Tooltip("Damage we pass to objects we damage impact.")]
        [SerializeField] protected float damage;

        [Tooltip("Flag to track if we have been used already, as to avoid multiple overlaps with multiple targets per frame.")]
        protected bool consumed = false;

        //note this may wish to be expanded to an audioeffect so or multiple clips for more interaction types.
        [SerializeField] protected AudioClip clipToPlayOnFire;

        public float FireRate => fireRate;
        public AudioClip ClipToPlayOnFire => clipToPlayOnFire;
        public Rigidbody2D Rigidbody2D => rb;
        public GameObject SpawnEffect => spawnEffect;

        public virtual void Start()
        {
            if (rb == null)
            {
                rb = GetComponent<Rigidbody2D>();
            }

            //default to moving up the screen, so rotating spawn point will aim us
            rb.velocity += (Vector2)(transform.up * speed);
            StartCoroutine(Despawn());
        }

        public virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (consumed)
                return;

            ProcessDamage(other, damage);

            //impact doesn't require rbs so just check the col tag
            if (impactTags.Contains(other.tag))
            {
                ImpactDespawnNow();
            }
        }

        public virtual void ProcessDamage(Collider2D other, float dmg)
        {
            //only other rbs have hp
            if (other.attachedRigidbody != null && dealDamageTags.Contains(other.attachedRigidbody.tag))
            {
                //fetch hp
                var otherHP = other.attachedRigidbody.GetComponent<Health>();
                if (otherHP != null)
                {
                    //if valid deal it
                    otherHP.ReceiveDamage(dmg);
                }
            }
        }

        private IEnumerator Despawn()
        {
            yield return new WaitForSeconds(TTL);
            DespawnNow();
        }

        public virtual void DespawnNow()
        {
            if (despawnEffect != null)
            {
                Instantiate(despawnEffect, transform.position, Quaternion.identity);
            }

            Destroy(gameObject);
        }

        public virtual void ImpactDespawnNow()
        {
            if (impactEffect != null)
            {
                //we may in future wish to track the direction of the impact so the effect can be rotated
                Instantiate(impactEffect, transform.position, Quaternion.identity);
            }

            //we have chosen to play both effects, this may not be your desire
            DespawnNow();

            //prevent this projectile from being used again, prevents multiple overlaps on the same frame
            consumed = true;
        }
    }
}
