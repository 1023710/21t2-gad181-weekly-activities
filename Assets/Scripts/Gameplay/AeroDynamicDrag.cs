﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Simulate drag/air resistance based on facing and direction of travel.
    /// 
    /// If you wish to make this frame rate independent follow up 
    /// https://www.gamasutra.com/blogs/ScottLembcke/20180404/316046/Improved_Lerp_Smoothing.php
    /// </summary>
    public class AeroDynamicDrag : MonoBehaviour
    {
        [SerializeField] protected Rigidbody2D rb;
        [SerializeField] protected float coeffOfFriction;
        [SerializeField] protected float frontalArea = 1;
        [SerializeField] protected AnimationCurve dirToAreaScale = AnimationCurve.Linear(0, 1, 1, 1);
        [SerializeField] protected float velocityRaisedToThePower = 1; //'true' value here is 2 but 1 tends to be more luftrausers-ey

        [Tooltip("kg/m^3")] [SerializeField] protected float airDensity = 1.29f;

        private void FixedUpdate()
        {
            var dirArea = frontalArea * dirToAreaScale.Evaluate(Mathf.Abs(Vector2.Dot(rb.velocity.normalized, transform.up)));

            //drag = 1/2 * cD * density * facingArea * vel^2

            var dForce = 0.5f * coeffOfFriction * airDensity * dirArea * Mathf.Pow(rb.velocity.magnitude, velocityRaisedToThePower);
            rb.AddForce(-dForce * rb.velocity.normalized);
        }
    }
}
