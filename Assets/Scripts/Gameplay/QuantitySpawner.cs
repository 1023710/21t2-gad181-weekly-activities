﻿using System.Collections.Generic;
using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Maintains a given number of objects, via the use of GameObjectCollection. Spawn locations are determined via
    /// SpawnLocationProviders.
    /// </summary>
    public class QuantitySpawner : MonoBehaviour
    {
        [System.Serializable]
        public struct LevelInfo
        {
            public int desiredQuantity;
            public float respawnDelay;
        }

        [SerializeField] protected GameObjectCollectionSO targetCollection;
        [SerializeField] protected GameObject prefabToSpawn;

        [SerializeField] protected int level = 0;
        [SerializeField] protected LevelInfo[] levelSpawnInfo;

        protected List<float> timers = new List<float>();
        [SerializeField] protected SpawnLocationProvider spawnLocationProvider;

        [SerializeField] protected Camera targetCamera;

        private void Start()
        {
            if (targetCamera == null)
                targetCamera = Camera.main;
        }

        private void Update()
        {
            AddNewTimersIfBelowTarget();
            CountDownAllTimers();
        }

        private void CountDownAllTimers()
        {
            for (int i = timers.Count - 1; i >= 0; i--)
            {
                timers[i] -= Time.deltaTime;
                if (timers[i] < 0)
                {
                    Spawn();
                    timers.RemoveAt(i);
                }
            }
        }

        private void AddNewTimersIfBelowTarget()
        {
            if (timers.Count + targetCollection.Count < levelSpawnInfo[level].desiredQuantity)
            {
                timers.Add(levelSpawnInfo[level].respawnDelay);
            }
        }

        protected virtual void Spawn()
        {
            spawnLocationProvider.InitPreFetch(prefabToSpawn.GetComponent<SpriteRenderer>().bounds.extents, targetCamera);

            var givenLocation = spawnLocationProvider.FetchCameraSafeRandom(targetCamera);

            Instantiate(prefabToSpawn, givenLocation, Quaternion.identity);
        }
    }
}
