﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Dumby player controller that only handles weapon firing at present, used in Weapon Testing scene.
    ///
    /// Structure here to indicate that gather input is separate from taking action on player intention.
    /// </summary>
    public class DumbyPlayerController : MonoBehaviour
    {
        public Weapon weapon;
        public KeyCode fireKey = KeyCode.X;

        public virtual void Update()
        {
            if (Input.GetKey(fireKey))
            {
                weapon.Fire();
            }
        }
    }
}
