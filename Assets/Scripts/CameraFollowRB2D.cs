using UnityEngine;

namespace SAE.Example
{
    public class CameraFollowRB2D : MonoBehaviour
    {
        [SerializeField] protected Transform cameraTransform;
        [SerializeField] protected Rigidbody2D targetRB2D;
        //[SerializeField] protected Vector3 cameraOffset = new Vector3(0, 0, -10);
        [SerializeField] protected float speedSmooth = 10f;
        [SerializeField] protected Vector3 defDis = new Vector3(0, 0, -10f);

        public Vector3 velocity = Vector3.zero;


        void LateUpdate() // Late update to avoid potential "Jittering issues" if the player is moved around the scene seperate from player controls.
        {
            //LerpFollow();
            SmoothDampFollow();
        }

        public void LerpFollow() // Camera follow based on Vector3.Lerp
<<<<<<< HEAD
        {
            // Assign the target of the camera and the offset so the ship is still in view.
            //Vector3 desiredPosition = targetRB2D.transform.position + cameraOffset; 

            // Using the lerp function calculate a point between where the camera currently is and where the ship is currently positioned
            // Based on the speed set in the inspector for the camera to start following to move the player.
            //Vector3 smoothedFollow = Vector3.Lerp(transform.position, desiredPosition, speedSmooth * Time.deltaTime);

            //cameraTransform.position = smoothedFollow; // Move the camera towards the calculated point.
        }

        public void SmoothDampFollow()
        {
=======
        {
            // Assign the target of the camera and the offset so the ship is still in view.
            //Vector3 desiredPosition = targetRB2D.transform.position + cameraOffset; 

            // Using the lerp function calculate a point between where the camera currently is and where the ship is currently positioned
            // Based on the speed set in the inspector for the camera to start following to move the player.
            //Vector3 smoothedFollow = Vector3.Lerp(transform.position, desiredPosition, speedSmooth * Time.deltaTime);

            //cameraTransform.position = smoothedFollow; // Move the camera towards the calculated point.
        }

        public void SmoothDampFollow()
        {
>>>>>>> 039800b8692fe9e61c7e76f8c06198c1dbcda651
            //if (targetRB2D.transform.rotation.z >0)
            //{
            //    defDis = new Vector3(-2f, 0f, -10f);
            //}
            //else if (targetRB2D.transform.rotation.z <0)
            //{
            //    defDis = new Vector3(2f, 0f, -10f);
            //}
<<<<<<< HEAD

            Vector3 targetPosition = targetRB2D.transform.TransformPoint(targetRB2D.transform.rotation * defDis);
            Debug.Log(targetPosition);
            Vector3 currentPosition = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, speedSmooth * Time.deltaTime);
=======
            
            Vector3 targetPosition = targetRB2D.transform.TransformPoint(targetRB2D.transform.rotation * defDis);
            Debug.Log(targetPosition);
            Vector3 currentPosition = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, speedSmooth*Time.deltaTime);
>>>>>>> 039800b8692fe9e61c7e76f8c06198c1dbcda651
            transform.position = currentPosition;
        }
    }
}